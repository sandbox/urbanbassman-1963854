<?php
/**
 * @file
 * cit_docs_views.views_default.inc
 */

/**
 * Implementation of hook_views_default_views().
 */
function cit_docs_views_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'home';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Home';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'grid';
  $handler->display->display_options['style_options']['grouping'] = '';
  $handler->display->display_options['style_options']['summary'] = 'List of publications';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Icon */
  $handler->display->display_options['fields']['field_icon']['id'] = 'field_icon';
  $handler->display->display_options['fields']['field_icon']['table'] = 'field_data_field_icon';
  $handler->display->display_options['fields']['field_icon']['field'] = 'field_icon';
  $handler->display->display_options['fields']['field_icon']['label'] = '';
  $handler->display->display_options['fields']['field_icon']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_icon']['hide_alter_empty'] = FALSE;
  /* Field: Content: Publication link */
  $handler->display->display_options['fields']['field_doc_link']['id'] = 'field_doc_link';
  $handler->display->display_options['fields']['field_doc_link']['table'] = 'field_data_field_doc_link';
  $handler->display->display_options['fields']['field_doc_link']['field'] = 'field_doc_link';
  $handler->display->display_options['fields']['field_doc_link']['label'] = '';
  $handler->display->display_options['fields']['field_doc_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_doc_link']['hide_alter_empty'] = FALSE;
  /* Field: Content: Message */
  $handler->display->display_options['fields']['field_message']['id'] = 'field_message';
  $handler->display->display_options['fields']['field_message']['table'] = 'field_data_field_message';
  $handler->display->display_options['fields']['field_message']['field'] = 'field_message';
  $handler->display->display_options['fields']['field_message']['label'] = '';
  $handler->display->display_options['fields']['field_message']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_message']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_message']['hide_alter_empty'] = FALSE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['element_wrapper_class'] = 'pub-summary';
  $handler->display->display_options['fields']['body']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['body']['hide_alter_empty'] = FALSE;
  /* Field: Content: Family */
  $handler->display->display_options['fields']['field_family']['id'] = 'field_family';
  $handler->display->display_options['fields']['field_family']['table'] = 'field_data_field_family';
  $handler->display->display_options['fields']['field_family']['field'] = 'field_family';
  $handler->display->display_options['fields']['field_family']['label'] = '';
  $handler->display->display_options['fields']['field_family']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_family']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_family']['hide_alter_empty'] = FALSE;
  /* Sort criterion: Content: Sort Order (field_sort) */
  $handler->display->display_options['sorts']['field_sort_value']['id'] = 'field_sort_value';
  $handler->display->display_options['sorts']['field_sort_value']['table'] = 'field_data_field_sort';
  $handler->display->display_options['sorts']['field_sort_value']['field'] = 'field_sort_value';
  /* Sort criterion: Content: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 0;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'publication' => 'publication',
  );
  $handler->display->display_options['filters']['type']['group'] = '0';

  /* Display: Home Page */
  $handler = $view->new_display('page', 'Home Page', 'page');
  $handler->display->display_options['display_description'] = 'Home Page';
  $handler->display->display_options['path'] = 'home';

  /* Display: User Guides */
  $handler = $view->new_display('page', 'User Guides', 'page_1');
  $handler->display->display_options['display_description'] = 'List of User Guides';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Icon */
  $handler->display->display_options['fields']['field_icon']['id'] = 'field_icon';
  $handler->display->display_options['fields']['field_icon']['table'] = 'field_data_field_icon';
  $handler->display->display_options['fields']['field_icon']['field'] = 'field_icon';
  $handler->display->display_options['fields']['field_icon']['label'] = '';
  $handler->display->display_options['fields']['field_icon']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_icon']['hide_alter_empty'] = FALSE;
  /* Field: Content: Publication link */
  $handler->display->display_options['fields']['field_doc_link']['id'] = 'field_doc_link';
  $handler->display->display_options['fields']['field_doc_link']['table'] = 'field_data_field_doc_link';
  $handler->display->display_options['fields']['field_doc_link']['field'] = 'field_doc_link';
  $handler->display->display_options['fields']['field_doc_link']['label'] = '';
  $handler->display->display_options['fields']['field_doc_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_doc_link']['hide_alter_empty'] = FALSE;
  /* Field: Content: Message */
  $handler->display->display_options['fields']['field_message']['id'] = 'field_message';
  $handler->display->display_options['fields']['field_message']['table'] = 'field_data_field_message';
  $handler->display->display_options['fields']['field_message']['field'] = 'field_message';
  $handler->display->display_options['fields']['field_message']['label'] = '';
  $handler->display->display_options['fields']['field_message']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_message']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_message']['hide_alter_empty'] = FALSE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['element_wrapper_class'] = 'pub-summary';
  $handler->display->display_options['fields']['body']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['body']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'publication' => 'publication',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Family (field_family) */
  $handler->display->display_options['filters']['field_family_value']['id'] = 'field_family_value';
  $handler->display->display_options['filters']['field_family_value']['table'] = 'field_data_field_family';
  $handler->display->display_options['filters']['field_family_value']['field'] = 'field_family_value';
  $handler->display->display_options['filters']['field_family_value']['value'] = 'User Guides';
  $handler->display->display_options['filters']['field_family_value']['group'] = 1;
  $handler->display->display_options['path'] = 'publications/user-guides';

  /* Display: Implementation Guides */
  $handler = $view->new_display('page', 'Implementation Guides', 'page_2');
  $handler->display->display_options['display_description'] = 'Implementation Guides';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Icon */
  $handler->display->display_options['fields']['field_icon']['id'] = 'field_icon';
  $handler->display->display_options['fields']['field_icon']['table'] = 'field_data_field_icon';
  $handler->display->display_options['fields']['field_icon']['field'] = 'field_icon';
  $handler->display->display_options['fields']['field_icon']['label'] = '';
  $handler->display->display_options['fields']['field_icon']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_icon']['hide_alter_empty'] = FALSE;
  /* Field: Content: Publication link */
  $handler->display->display_options['fields']['field_doc_link']['id'] = 'field_doc_link';
  $handler->display->display_options['fields']['field_doc_link']['table'] = 'field_data_field_doc_link';
  $handler->display->display_options['fields']['field_doc_link']['field'] = 'field_doc_link';
  $handler->display->display_options['fields']['field_doc_link']['label'] = '';
  $handler->display->display_options['fields']['field_doc_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_doc_link']['hide_alter_empty'] = FALSE;
  /* Field: Content: Message */
  $handler->display->display_options['fields']['field_message']['id'] = 'field_message';
  $handler->display->display_options['fields']['field_message']['table'] = 'field_data_field_message';
  $handler->display->display_options['fields']['field_message']['field'] = 'field_message';
  $handler->display->display_options['fields']['field_message']['label'] = '';
  $handler->display->display_options['fields']['field_message']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_message']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_message']['hide_alter_empty'] = FALSE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['element_wrapper_class'] = 'pub-summary';
  $handler->display->display_options['fields']['body']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['body']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'publication' => 'publication',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Family (field_family) */
  $handler->display->display_options['filters']['field_family_value']['id'] = 'field_family_value';
  $handler->display->display_options['filters']['field_family_value']['table'] = 'field_data_field_family';
  $handler->display->display_options['filters']['field_family_value']['field'] = 'field_family_value';
  $handler->display->display_options['filters']['field_family_value']['value'] = 'Implementation Guides';
  $handler->display->display_options['filters']['field_family_value']['group'] = 1;
  $handler->display->display_options['path'] = 'publications/implementation-guides';

  /* Display: System Administration Guides */
  $handler = $view->new_display('page', 'System Administration Guides', 'page_3');
  $handler->display->display_options['display_description'] = 'System Administration Guides';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Icon */
  $handler->display->display_options['fields']['field_icon']['id'] = 'field_icon';
  $handler->display->display_options['fields']['field_icon']['table'] = 'field_data_field_icon';
  $handler->display->display_options['fields']['field_icon']['field'] = 'field_icon';
  $handler->display->display_options['fields']['field_icon']['label'] = '';
  $handler->display->display_options['fields']['field_icon']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_icon']['hide_alter_empty'] = FALSE;
  /* Field: Content: Publication link */
  $handler->display->display_options['fields']['field_doc_link']['id'] = 'field_doc_link';
  $handler->display->display_options['fields']['field_doc_link']['table'] = 'field_data_field_doc_link';
  $handler->display->display_options['fields']['field_doc_link']['field'] = 'field_doc_link';
  $handler->display->display_options['fields']['field_doc_link']['label'] = '';
  $handler->display->display_options['fields']['field_doc_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_doc_link']['hide_alter_empty'] = FALSE;
  /* Field: Content: Message */
  $handler->display->display_options['fields']['field_message']['id'] = 'field_message';
  $handler->display->display_options['fields']['field_message']['table'] = 'field_data_field_message';
  $handler->display->display_options['fields']['field_message']['field'] = 'field_message';
  $handler->display->display_options['fields']['field_message']['label'] = '';
  $handler->display->display_options['fields']['field_message']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_message']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_message']['hide_alter_empty'] = FALSE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['element_wrapper_class'] = 'pub-summary';
  $handler->display->display_options['fields']['body']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['body']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'publication' => 'publication',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Family (field_family) */
  $handler->display->display_options['filters']['field_family_value']['id'] = 'field_family_value';
  $handler->display->display_options['filters']['field_family_value']['table'] = 'field_data_field_family';
  $handler->display->display_options['filters']['field_family_value']['field'] = 'field_family_value';
  $handler->display->display_options['filters']['field_family_value']['value'] = 'System Administration Guides';
  $handler->display->display_options['filters']['field_family_value']['group'] = 1;
  $handler->display->display_options['path'] = 'publications/system-administration-guides';

  /* Display: New */
  $handler = $view->new_display('page', 'New', 'page_5');
  $handler->display->display_options['display_description'] = 'New publications';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Icon */
  $handler->display->display_options['fields']['field_icon']['id'] = 'field_icon';
  $handler->display->display_options['fields']['field_icon']['table'] = 'field_data_field_icon';
  $handler->display->display_options['fields']['field_icon']['field'] = 'field_icon';
  $handler->display->display_options['fields']['field_icon']['label'] = '';
  $handler->display->display_options['fields']['field_icon']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_icon']['hide_alter_empty'] = FALSE;
  /* Field: Content: Publication link */
  $handler->display->display_options['fields']['field_doc_link']['id'] = 'field_doc_link';
  $handler->display->display_options['fields']['field_doc_link']['table'] = 'field_data_field_doc_link';
  $handler->display->display_options['fields']['field_doc_link']['field'] = 'field_doc_link';
  $handler->display->display_options['fields']['field_doc_link']['label'] = '';
  $handler->display->display_options['fields']['field_doc_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_doc_link']['hide_alter_empty'] = FALSE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['element_wrapper_class'] = 'pub-summary';
  $handler->display->display_options['fields']['body']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['body']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'publication' => 'publication',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Message (field_message) */
  $handler->display->display_options['filters']['field_message_value']['id'] = 'field_message_value';
  $handler->display->display_options['filters']['field_message_value']['table'] = 'field_data_field_message';
  $handler->display->display_options['filters']['field_message_value']['field'] = 'field_message_value';
  $handler->display->display_options['filters']['field_message_value']['operator'] = 'word';
  $handler->display->display_options['filters']['field_message_value']['value'] = 'New! Updated!';
  $handler->display->display_options['filters']['field_message_value']['group'] = 1;
  $handler->display->display_options['path'] = 'publications/new';
  $export['home'] = $view;

  $view = new view;
  $view->name = 'user_guides';
  $view->description = 'User Guides';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'User Guides';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Publications';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['grouping'] = 'field_publication';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Publication */
  $handler->display->display_options['fields']['field_publication']['id'] = 'field_publication';
  $handler->display->display_options['fields']['field_publication']['table'] = 'field_data_field_publication';
  $handler->display->display_options['fields']['field_publication']['field'] = 'field_publication';
  $handler->display->display_options['fields']['field_publication']['label'] = '';
  $handler->display->display_options['fields']['field_publication']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_publication']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_publication']['hide_alter_empty'] = FALSE;
  /* Sort criterion: Content: Publication (field_publication) */
  $handler->display->display_options['sorts']['field_publication_value']['id'] = 'field_publication_value';
  $handler->display->display_options['sorts']['field_publication_value']['table'] = 'field_data_field_publication';
  $handler->display->display_options['sorts']['field_publication_value']['field'] = 'field_publication_value';
  /* Sort criterion: Content: Chapter (field_chapter_number) */
  $handler->display->display_options['sorts']['field_chapter_number_value']['id'] = 'field_chapter_number_value';
  $handler->display->display_options['sorts']['field_chapter_number_value']['table'] = 'field_data_field_chapter_number';
  $handler->display->display_options['sorts']['field_chapter_number_value']['field'] = 'field_chapter_number_value';
  /* Contextual filter: Content: Publication (field_publication) */
  $handler->display->display_options['arguments']['field_publication_value']['id'] = 'field_publication_value';
  $handler->display->display_options['arguments']['field_publication_value']['table'] = 'field_data_field_publication';
  $handler->display->display_options['arguments']['field_publication_value']['field'] = 'field_publication_value';
  $handler->display->display_options['arguments']['field_publication_value']['default_action'] = 'summary';
  $handler->display->display_options['arguments']['field_publication_value']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_publication_value']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_publication_value']['summary']['format'] = 'unformatted_summary';
  $handler->display->display_options['arguments']['field_publication_value']['summary_options']['count'] = FALSE;
  $handler->display->display_options['arguments']['field_publication_value']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_publication_value']['limit'] = '0';
  $handler->display->display_options['arguments']['field_publication_value']['path_case'] = 'lower';
  $handler->display->display_options['arguments']['field_publication_value']['transform_dash'] = TRUE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 0;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'topic' => 'topic',
  );
  /* Filter criterion: Content: Publication (field_publication) */
  $handler->display->display_options['filters']['field_publication_value']['id'] = 'field_publication_value';
  $handler->display->display_options['filters']['field_publication_value']['table'] = 'field_data_field_publication';
  $handler->display->display_options['filters']['field_publication_value']['field'] = 'field_publication_value';
  $handler->display->display_options['filters']['field_publication_value']['operator'] = '!=';
  $handler->display->display_options['filters']['field_publication_value']['value'] = 'QuercusLive for Students Administrator’s Guide';
  $handler->display->display_options['filters']['field_publication_value']['group'] = '0';

  /* Display: Publications */
  $handler = $view->new_display('page', 'Publications', 'page');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['path'] = 'publications';
  $handler->display->display_options['menu']['title'] = 'Publications';
  $handler->display->display_options['menu']['description'] = 'Publications';
  $handler->display->display_options['menu']['weight'] = '10';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;

  /* Display: PubList */
  $handler = $view->new_display('block', 'PubList', 'block_1');
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['css_class'] = 'publist';
  $handler->display->display_options['display_description'] = 'Publications List';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: Publication (field_publication) */
  $handler->display->display_options['arguments']['field_publication_value']['id'] = 'field_publication_value';
  $handler->display->display_options['arguments']['field_publication_value']['table'] = 'field_data_field_publication';
  $handler->display->display_options['arguments']['field_publication_value']['field'] = 'field_publication_value';
  $handler->display->display_options['arguments']['field_publication_value']['default_action'] = 'summary';
  $handler->display->display_options['arguments']['field_publication_value']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_publication_value']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_publication_value']['summary']['format'] = 'unformatted_summary';
  $handler->display->display_options['arguments']['field_publication_value']['summary_options']['count'] = FALSE;
  $handler->display->display_options['arguments']['field_publication_value']['summary_options']['items_per_page'] = '50';
  $handler->display->display_options['arguments']['field_publication_value']['limit'] = '0';
  $handler->display->display_options['arguments']['field_publication_value']['path_case'] = 'lower';
  $handler->display->display_options['arguments']['field_publication_value']['transform_dash'] = TRUE;

  /* Display: ChapList */
  $handler = $view->new_display('block', 'ChapList', 'block_2');
  $handler->display->display_options['enabled'] = FALSE;
  $handler->display->display_options['display_description'] = 'Chapter List';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: Title */
  $handler->display->display_options['arguments']['title']['id'] = 'title';
  $handler->display->display_options['arguments']['title']['table'] = 'node';
  $handler->display->display_options['arguments']['title']['field'] = 'title';
  $handler->display->display_options['arguments']['title']['default_action'] = 'summary';
  $handler->display->display_options['arguments']['title']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['title']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['title']['summary']['format'] = 'unformatted_summary';
  $handler->display->display_options['arguments']['title']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['title']['limit'] = '0';
  $export['user_guides'] = $view;

  return $export;
}
